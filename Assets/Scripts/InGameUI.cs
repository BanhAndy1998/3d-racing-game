﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour

{
    public FixedJoystick joystick;
    public GameObject player;
    public Text playerVelocity;
    public Text lapCount;
    public Text songInfo;

    private void Start()
    {
        Debug.Log(Application.platform);
        if (Application.isMobilePlatform)
        {
            Debug.Log("mobile");
            joystick.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerInfo.Instance.currSong == 0) {
            songInfo.text = "NC feat NRGFACTORY - Waiting";
        }
        if (PlayerInfo.Instance.currSong == 1) {
            songInfo.text = "KO3 - Soda Galaxy";
        }
        if (PlayerInfo.Instance.currSong == 2) {
            songInfo.text = "NC feat NRGFACTORY - Reach the sky, without you";
        }
        if (PlayerInfo.Instance.currSong == 3) {
            songInfo.text = "Syunn - Ace Out";
        }
        if (PlayerInfo.Instance.currSong == 4) {
            songInfo.text = "Camellia - Insecticide";
        }
        if (PlayerInfo.Instance.currSong == 5) {
            songInfo.text = "Camellia - Galaxy Burst";
        }
        if (PlayerInfo.Instance.currSong == 6) {
            songInfo.text = "Camellia - Bangin' Burst";
        }

        playerVelocity.text = ((int) (player.GetComponent<Rigidbody>().velocity.magnitude*4)).ToString() + " MPH";
        lapCount.text = "Lap " + LapTracker.lapTracker + "/" + LapTracker.maxLaps;
    }
}
