﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CreditsScript : MonoBehaviour
{
    public AudioSource music;
    void Awake()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        music.volume = PlayerInfo.Instance.music;
    }

    public void menu()
    {
        SceneManager.LoadScene("Title");
    }
}
