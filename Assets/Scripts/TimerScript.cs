﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TimerScript : MonoBehaviour
{
    bool paused = false;
    public float time;
    public float timeElapsed;
    public float lastLapTime;  
    public Text lapPBText;
    public Text stagePBText;
    public Text lapText;
    public Text stageText;
    public Text finalTime;

    // Start is called before the first frame update
    void Start()
    {
        time = Time.time;
        lastLapTime = 0;
        stagePBText.text = "Stage Best: " + string.Format("{0:00}:{1:00}:{2:000}", System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 2] / 60), System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 2] % 60), PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 2] * 1000 % 1000);
        lapPBText.text = "Lap Best: " + string.Format("{0:00}:{1:00}:{2:000}", System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] / 60), System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] % 60), PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] * 1000 % 1000);       
    }

    // Update is called once per frame
    void Update()
    {
        if (paused == false)
        {
            timeElapsed = Time.time - time;
            float minutes = (float)System.Math.Floor(timeElapsed / 60);
            float seconds = (float)System.Math.Floor(timeElapsed % 60);
            float milliseconds = timeElapsed * 1000 % 1000;
            stageText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
        }
    }

    public void LapTimeSave(int stage)
    {
        lastLapTime = timeElapsed - lastLapTime;
        float minutes = (float)System.Math.Floor(lastLapTime / 60);
        float seconds = (float)System.Math.Floor(lastLapTime % 60);
        float milliseconds = lastLapTime * 1000 % 1000;
        lapText.text = string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
        if(lastLapTime < System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1])) {
            PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] = lastLapTime;
            if(milliseconds>=500)
            {
                lastLapTime += 1;
            }
            lapPBText.text = "Lap Best: " + string.Format("{0:00}:{1:00}:{2:000}", System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] / 60), System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] % 60), PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 1] * 1000 % 1000);
        }
        if(LapTracker.lapTracker > LapTracker.maxLaps)
        {
            StageTimeSave(stage);
        }
    }

    public void StageTimeSave(int stage)
    {
        if (timeElapsed * 1000 % 1000 >= 500)
        {
            timeElapsed += 1;
        }
        finalTime.text = string.Format("{0:00}:{1:00}:{2:000}", System.Math.Floor(timeElapsed / 60), System.Math.Floor(timeElapsed % 60), timeElapsed * 1000 % 1000);
        if (timeElapsed < System.Math.Floor(PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 2]))
        {
            PlayerInfo.Instance.times[PlayerInfo.Instance.currStage * 2 - 2] = timeElapsed;
        }
    }

}
