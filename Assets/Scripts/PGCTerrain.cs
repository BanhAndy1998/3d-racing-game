﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PGCTerrain : MonoBehaviour

{
    public Terrain terrain;
    public TerrainData terrainData;
    public float width = 10f;
    public float height = 10f;
    public float depth = 1f;
    public Vector3 heightMapScale = new Vector3(10, 10, 10);
    public int layers;     //number of layers of perlin noise

    void OnEnable()
    {
        terrain = this.GetComponent<Terrain>();
        terrainData = Terrain.activeTerrain.terrainData;

    }

    public void RandomTerrain()
    {
        terrainData.heightmapResolution = (int)height;
        terrainData.size = new Vector3(height, depth, width);
        float[,] heightMap = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];
        for (int x = 0; x < terrainData.heightmapResolution; x++)
        {
            for (int y = 0; y < terrainData.heightmapResolution; y++)
            {
                heightMap[x, y] = Random.Range(0f, 1f);
            }
        }
        terrainData.SetHeights(0, 0, heightMap);
    }

    public void PerlinTerrain()
    {
        terrainData.heightmapResolution = (int)height;
        terrainData.size = new Vector3(height, depth, width);
        float offsetX = Random.Range(0f, 1000f);
        float offsetY = Random.Range(0f, 1000f);
        float[,] heightMap = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];
        for (int x = 0; x < terrainData.heightmapResolution; x++)
        {
            for (int y = 0; y < terrainData.heightmapResolution; y++)
            {
                
                float xCoord = (float)x / terrainData.heightmapResolution * heightMapScale.x + offsetX;
                float yCoord = (float)y / terrainData.heightmapResolution * heightMapScale.y + offsetY;
                heightMap[x, y] = Mathf.PerlinNoise(xCoord, yCoord);
            }
        }
        terrainData.SetHeights(0, 0, heightMap);
    }

    public void MultiPerlinTerrain()
    {
        terrainData.heightmapResolution = (int)height;
        terrainData.size = new Vector3(height, depth, width);
        float offsetX = Random.Range(0f, 1000f);
        float offsetY = Random.Range(0f, 1000f);
        float[,] heightMap = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];
        float maxNoiseHeight = float.MinValue;
        float minNoiseHeight = float.MaxValue;

        for (int x = 0; x < terrainData.heightmapResolution; x++)
        {
            for (int y = 0; y < terrainData.heightmapResolution; y++)
            {
                float noiseHeight = 0;
                float amplitude = 1;
                float frequency = 1;
                for(int i = 0; i < layers; i++)
                {
                    float xCoord = (float)x / terrainData.heightmapResolution * heightMapScale.x + offsetX;
                    float yCoord = (float)y / terrainData.heightmapResolution * heightMapScale.y + offsetY;
                    float perlin = Mathf.PerlinNoise(xCoord, yCoord) * 2f - 1f;
                    noiseHeight += perlin * amplitude;
                    amplitude *= 0.5f;
                    frequency *= 2f;
                }


                if (noiseHeight > maxNoiseHeight)
                {
                    maxNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minNoiseHeight)
                {
                    minNoiseHeight = noiseHeight;
                }
                heightMap[x, y] = noiseHeight; 
            }
        }

        for (int x = 0; x < terrainData.heightmapResolution; x++)
        {
            for (int y = 0; y < terrainData.heightmapResolution; y++)
            {
                heightMap[x, y] = Mathf.InverseLerp(minNoiseHeight, maxNoiseHeight, heightMap[x, y]);
            }
        }
        terrainData.SetHeights(0, 0, heightMap);
    }

    public void SineTerrain()
    {
        terrainData.heightmapResolution = (int)height;
        terrainData.size = new Vector3(height, depth, width);
        float[,] heightMap = new float[terrainData.heightmapResolution, terrainData.heightmapResolution];
        for (int x = 0; x < terrainData.heightmapResolution; x++)
        {
            for (int y = 0; y < terrainData.heightmapResolution; y++)
            {
                
                heightMap[x, y] = Mathf.Sin(x + y);
            }
        }
        terrainData.SetHeights(0, 0, heightMap);
    }
}
