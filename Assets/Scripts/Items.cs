﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Items : MonoBehaviour
{
    public Rigidbody rb;
    public float boost = 1000000;
    public Vector3 com = new Vector3(0, -2, 0); //center of mass
    public GameObject spawn;
    public GameObject spawn2;
    public GameObject spawn3;
    public GameObject spawn4;
    public LapTracker lapTracker;

    private void Start()
    {
        rb.centerOfMass = com;
    }

    private void Update()
    {
        //Debug.Log(rb.velocity.magnitude);
        if (Input.GetKey(KeyCode.R))
        {
            /*transform.eulerAngles = new Vector3(0, -90, 0);
            rb.velocity = Vector3.zero;
            transform.position = spawn.transform.position;
            Debug.Log("Respawn Car"); */
            Respawn();
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "PowerUp")
        {
            StartCoroutine(Boost());
        }

        if (other.transform.tag == "ShortCut")
        {
            transform.eulerAngles = new Vector3(-50, 180, 0);
            //.AddForce(transform.forward * boost);
        }

        if (other.transform.tag == "Reset")
        {
            Respawn();    
            Debug.Log("passed");
        }
    }

    IEnumerator Boost()
    {
        rb.AddForce(transform.forward * boost);

        yield return new WaitForSeconds(3);
    }

    private void Respawn()
    {
        rb.velocity = Vector3.zero;
        switch (lapTracker.lastCheckpoint)
        {
            case 0:
                transform.eulerAngles = spawn.transform.eulerAngles;
                transform.position = spawn.transform.position;
                break;
            case 1:
                transform.eulerAngles = spawn2.transform.eulerAngles;
                transform.position = spawn2.transform.position;
                break;
            case 2:
                transform.eulerAngles = spawn3.transform.eulerAngles;
                transform.position = spawn3.transform.position;
                break;
            case 3:
                transform.eulerAngles = spawn4.transform.eulerAngles;
                transform.position = spawn4.transform.position;
                break;
            default:
                transform.eulerAngles = spawn.transform.eulerAngles;
                transform.position = spawn.transform.position;
                break;
        }
    }
}