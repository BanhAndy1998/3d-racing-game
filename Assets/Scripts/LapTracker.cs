﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LapTracker : MonoBehaviour
{
    //attatch to the player and AI cars
    //will have 4 points the player will drive through
    //this is basically to stop people from being able to move back and forth through the finish line to increase their laps

    //when we pass through a "point" we will set the Reached boolean to true
    //you cannot set point three reached without 

    public bool finishReached = false;    //this is the start/finish line, when true reset the points to false
    public bool pointOneReached = false;    //put this 1/4 through the level
    public bool pointTwoReached = false;  //put this 2/4 through the level
    public bool pointThreeReached = false; //put this 3/4 through the level

    public TimerScript ts;
    public GameObject[] f2b;
    public Animator blackScreen;

    public int lastCheckpoint = 0; //0, 1, 2, or 3. Will be used to respawn the player at one of the points
    public static int lapTracker = 1;
    public static int maxLaps = 3;

    void Start()
    {
        f2b = GameObject.FindGameObjectsWithTag("F2B");
        foreach(GameObject x in f2b)
        {
            x.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PointOne")
        {
            lastCheckpoint = 1;
            Debug.Log("Point one reached");
            pointOneReached = true;
        }

        if (other.tag == "PointTwo")
        {
            if(pointOneReached == true)
            {
                lastCheckpoint = 2;
                Debug.Log("Point two reached");
                pointTwoReached = true;
            }
            else
            {
                Debug.Log("Reached point two before point one");
            }
        }

        if (other.tag == "PointThree")
        {
            if (pointTwoReached == true)
            {
                lastCheckpoint = 3;
                Debug.Log("Point three reached");
                pointThreeReached = true;
            }
            else
            {
                Debug.Log("Reached point three before point two");
            }
        }

        if (other.tag == "Finish")
        {
            Debug.Log("Finish Line Reached");


            if (pointThreeReached == true)
            {
                lastCheckpoint = 0;
                pointOneReached = false;
                pointTwoReached = false;
                pointThreeReached = false;
                lapTracker++;
                ts.LapTimeSave(PlayerInfo.Instance.currStage);
                Debug.Log("Lap counter increased to " + lapTracker);
            }
            if(lapTracker > maxLaps)
            {
                lapTracker = 3;
                Debug.Log("race over");
                StartCoroutine(Fade2Black());
                //load next scene
            }
        }
    }
    public IEnumerator Fade2Black()
    {
        foreach (GameObject x in f2b)
        {
            x.SetActive(true);
        }
        blackScreen.SetTrigger("Fade2Black");
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Title");
    }
}
