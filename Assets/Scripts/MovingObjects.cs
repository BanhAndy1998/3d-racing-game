﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjects : MonoBehaviour
{
    public float minX = 2f;
    public float maxX = 3f;
    public float minY = 2f;
    public float maxY = 3f;
    public float minZ = 2f;
    public float maxZ = 3f;
    public float speed = 10f;
    public float distancetogo = 15f;
    public bool moveX;
    public bool moveY;
    public bool moveZ;
    public bool carryPlayer;

    void Start()
    {
        minX = transform.position.x;
        maxX = transform.position.x + distancetogo;
        minY = transform.position.y;
        maxY = transform.position.y + distancetogo;
        minZ = transform.position.z;
        maxZ = transform.position.z + distancetogo;
    }

    // Update is called once per frame
    void Update()
    {
        if (moveX == true)
        {
            transform.position = new Vector3(Mathf.PingPong(Time.time * speed, maxX - minX) + minX, transform.position.y, transform.position.z);
        }

        if (moveY == true)
        {
            transform.position = new Vector3(transform.position.x, Mathf.PingPong(Time.time * speed, maxY - minY) + minY, transform.position.z);
        }

        if (moveZ == true)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.PingPong(Time.time * speed, maxZ - minZ) + minZ);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player" && carryPlayer == true)
        {
            var emptyObject = new GameObject();
            emptyObject.transform.parent = transform;
            collision.transform.parent = emptyObject.transform;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Player" && carryPlayer == true)
        {
            collision.transform.parent = null;
        }
    }
}
