﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{

    public GameObject player;
    public float mouseSensitivity = 5;
    public float freelookDistance = 10;
    float mouseX, mouseY;
    public bool freelook = false;
    public bool lookBehindPlayer = false;

    public float rotationSmoothTime;
    Vector3 rotationSmoothVelocity;
    Vector3 currentRotation;

    public float damp = 0.4f;

    public Vector3 offset = new Vector3(0, 4, 10);

    private void Start()
    {
        player = GameObject.Find("Player");
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;   //locks cursor to center of screen
        if (Application.isMobilePlatform)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }

    private void Update()
    {
        //while the user is holding space, they have freelook
        if (Input.GetKey(KeyCode.LeftShift))
        {
            if (PlayerInfo.Instance.freecam)
            {
                freelook = true;
            }
            else if (PlayerInfo.Instance.freecam == false)
            {
                lookBehindPlayer = true;
            }
        }
        else
        {
            lookBehindPlayer = false;
            freelook = false;
        }
    }

    private void FixedUpdate()
    {
        Vector3 newPosition = player.transform.TransformPoint(offset);
        transform.position = Vector3.Lerp(transform.position, newPosition, damp);
        transform.LookAt(player.transform);

        if (freelook)
        {
            //control freelook with mouse
            mouseX += Input.GetAxisRaw("Mouse X") * mouseSensitivity;
            mouseY += Input.GetAxisRaw("Mouse Y") * mouseSensitivity * -1;
            mouseY = Mathf.Clamp(mouseY, -20, 60);

            currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(mouseY, mouseX), ref rotationSmoothVelocity, rotationSmoothTime);
            transform.eulerAngles = currentRotation;
            transform.position = player.transform.position - transform.forward * freelookDistance;
        }
        if (lookBehindPlayer)
        {
            Vector3 behindOffset = new Vector3(0, 4, 20);
            Vector3 behindPosition = player.transform.TransformPoint(behindOffset);
            transform.position = Vector3.Lerp(transform.position, behindPosition, damp);
            transform.LookAt(player.transform);
        }
    }
}
