﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AICarMovement : MonoBehaviour
{
    public GameObject path; //path we set up
    public float maxSteerAngle;

    private List<Transform> waypoints; //list of the waypoints
    private int currentNode = 0; //index of the node we are currently at
    public float nodeRange;

    //wheels to apply the steering angle to
    public WheelCollider frontRight;
    public WheelCollider frontLeft;

    public float maxSpeed = 1000;
    public float currentSpeed = 0;
    public float turnSpeed = 5;

    public bool isBraking = false;
    public float brakeTorque = 100;
    public float brakingTime = 1f;

    public float raycastLength = 5f;
    public Vector3 frontRaycastPosition = new Vector3(0, 1, 0);
    public float frontSideRaycast = 1.4f;
    public float frontRaycastAngle = 30f;
    public bool avoiding = false;
    public float targetSteerAngle;
    

    // Start is called before the first frame update
    private void Start()
    {
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        waypoints = new List<Transform>();

        //adding the transforms of the waypoints from the Path object to the list of waypoints
        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                Debug.Log("Add Node" + i + " " + pathTransforms[i].position);
                waypoints.Add(pathTransforms[i]);
            }
        }
    }

    private void Update()
    {
        AvoidObstacles();
    }

    void FixedUpdate()
    {
        SmoothTurn();
        SteerCar();
        MoveCar();
        AdvanceWaypoint();
    }

    private void SteerCar()
    {
        //steering towards the waypoint
        if (!avoiding)
        {
            Vector3 relativeVector = transform.InverseTransformPoint(waypoints[currentNode].position);
            float steer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
            targetSteerAngle = steer;
        }
    }

    private void MoveCar()
    {
        currentSpeed = 2 * Mathf.PI * frontLeft.radius * frontLeft.rpm * 60 / 1000;
        if (currentSpeed < maxSpeed && !isBraking)
        {
            frontLeft.motorTorque = maxSpeed;
            frontRight.motorTorque = maxSpeed;
        }
        else
        {
            frontLeft.motorTorque = 0;
            frontRight.motorTorque = 0;
        }

        if(isBraking == true)
        {
            frontLeft.brakeTorque = brakeTorque;
            frontRight.brakeTorque = brakeTorque;
        }
        else
        {
            frontLeft.brakeTorque = 0;
            frontRight.brakeTorque = 0;
        }

    }

    //checking if we are near the waypoint in order to move on to the next one
    private void AdvanceWaypoint()
    {
        if (Vector3.Distance(transform.position, waypoints[currentNode].position) < nodeRange)
        {
            //StartCoroutine(brakingCoroutine());
            if (currentNode == waypoints.Count - 1)
            {
                currentNode = 0;
            }
            else
            {
                currentNode++;
            }
        }
    }

    //raycasts to avoid running into obstacles
    private void AvoidObstacles()
    {
        RaycastHit hit;
        Vector3 startRaycastPosition = transform.position;
        startRaycastPosition += transform.forward * frontRaycastPosition.z;
        startRaycastPosition += transform.up * frontRaycastPosition.y;
        float avoidMultiplier = 0;
        avoiding = false;
        
        //front right side raycast
        startRaycastPosition += transform.right * frontSideRaycast;
        if (Physics.Raycast(startRaycastPosition, transform.forward, out hit, raycastLength))
        {
            if (hit.collider.CompareTag("Avoid"))
            {
                avoiding = true;
                Debug.DrawLine(startRaycastPosition, hit.point);
                avoidMultiplier -= 1f;
            }
            
            //Debug.Log("We hit " + hit.collider.name + " " + hit.point);
        }
       
        //front right angle raycast
        else if (Physics.Raycast(startRaycastPosition, Quaternion.AngleAxis(frontRaycastAngle, transform.up) * transform.forward, out hit, raycastLength))
        {
            if (hit.collider.CompareTag("Avoid"))
            {
                avoiding = true;
                Debug.DrawLine(startRaycastPosition, hit.point);
                avoidMultiplier -= 0.5f;
            }
            
            //Debug.Log("We hit " + hit.collider.name + " " + hit.point);
        }

        //front left side raycast
        startRaycastPosition -= transform.right * frontSideRaycast * 2;
        if (Physics.Raycast(startRaycastPosition, transform.forward, out hit, raycastLength))
        {
            if (hit.collider.CompareTag("Avoid"))
            {
                avoidMultiplier += 1f;
                avoiding = true;
                Debug.DrawLine(startRaycastPosition, hit.point);
            }
        }
        
        //front left angle raycast
        else if (Physics.Raycast(startRaycastPosition, Quaternion.AngleAxis(-frontRaycastAngle, transform.up) * transform.forward, out hit, raycastLength))
        {
            if (hit.collider.CompareTag("Avoid"))
            {
                avoidMultiplier += 0.5f;
                avoiding = true;
                Debug.DrawLine(startRaycastPosition, hit.point);
            }           
        }

        //front center raycast
        if(avoidMultiplier == 0)
        {
            if (Physics.Raycast(startRaycastPosition, transform.forward, out hit, raycastLength))
            {
                if (hit.collider.CompareTag("Avoid"))
                {
                    avoiding = true;
                    Debug.DrawLine(startRaycastPosition, hit.point);
                    if (hit.normal.x < 0)
                    {
                        avoidMultiplier = -1;
                    }
                    else
                    {
                        avoidMultiplier = 1;
                    }
                }
            }
        }
       


        if (avoiding)
        {
            targetSteerAngle = maxSteerAngle * avoidMultiplier;
        }
    }

    private void SmoothTurn()
    {
        frontLeft.steerAngle = Mathf.Lerp(frontLeft.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
        frontRight.steerAngle = Mathf.Lerp(frontRight.steerAngle, targetSteerAngle, Time.deltaTime * turnSpeed);
    }

    IEnumerator brakingCoroutine()
    {
        isBraking = true;
        yield return new WaitForSeconds(1f);
        isBraking = false;
    }
}
