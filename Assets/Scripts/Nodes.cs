﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//visual way to see the waypoints the AI will move on
public class Nodes : MonoBehaviour
{
    public Color lineColor;
    private List<Transform> waypoints = new List<Transform>();

    private void OnDrawGizmos()
    {
        Gizmos.color = lineColor;
        Transform[] pathTransforms = GetComponentsInChildren<Transform>();
        waypoints = new List<Transform>();

        for(int i = 0; i < pathTransforms.Length; i++)
        {
            if(pathTransforms[i] != transform)
            {
                waypoints.Add(pathTransforms[i]);
            }
        }

        for (int i = 0; i < waypoints.Count; i++)
        {
            Vector3 currentNode = waypoints[i].position;
            Vector3 previousNode = Vector3.zero;
            if (i > 0)
            {
                previousNode = waypoints[i - 1].position;
            }
            else if (i == 0 && waypoints.Count > 1)
            {
                previousNode = waypoints[waypoints.Count - 1].position;
            }
            Gizmos.DrawLine(previousNode, currentNode);
            Gizmos.DrawWireSphere(currentNode, 0.3f);
        }
    }
}
