﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerInfo : MonoBehaviour
{
    private static PlayerInfo playerInfo = null;
    public bool tutorial;
    public int currSong = 0;
    public int currStage = 1;
    public float music;
    public float sfx;
    public bool freecam;
    public bool bangerSongs;
    public float[] times = new float[6];

    public static PlayerInfo Instance
    {
        get { return playerInfo; }
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this);
            playerInfo = this;
            for(int i = 0; i < 8; i++)
            {
                times[i] = (float)9999.999;
            }
        }
        else
        {
            if (playerInfo != this)
                Destroy(gameObject);
        }
    }

    public static void DestroyThis()
    {
        playerInfo = null;
    }
}
