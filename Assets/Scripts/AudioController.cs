﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioController : MonoBehaviour
{
    public AudioSource music;
    public AudioClip[] musicList = new AudioClip[7];
    public bool delay = true;
    public GameObject[] wheels = new GameObject[4];
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject x in wheels)
        {
            x.GetComponent<AudioSource>().volume = PlayerInfo.Instance.sfx;
        }
            music.volume = PlayerInfo.Instance.music;
        StartCoroutine(playSong());
    }

    void Update()
    {
        if (!music.isPlaying && !delay)
        {
            delay = true;
            PlayerInfo.Instance.currSong += 1;
            if (!PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong > 3)
                PlayerInfo.Instance.currSong = 0;
            if (PlayerInfo.Instance.currSong > 6)
            {
                PlayerInfo.Instance.currSong = 0;
            }
            StartCoroutine(playSong());
        }
        if (Input.GetKeyDown(KeyCode.P) && !delay) {
            delay = true;
            PlayerInfo.Instance.currSong += 1;
            if (!PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong > 3)
                PlayerInfo.Instance.currSong = 0;
            if (PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong > 6)
            {
                PlayerInfo.Instance.currSong = 0;
            }
            StartCoroutine(playSong());
        }
        if (Input.GetKeyDown(KeyCode.O) && !delay ) {
            delay = true;
            PlayerInfo.Instance.currSong -= 1;
            if (!PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong < 0)
                PlayerInfo.Instance.currSong = 3;
            if (PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong < 0)
            {
                PlayerInfo.Instance.currSong = 6;
            }
            StartCoroutine(playSong());
        }
        if(Input.GetKeyDown(KeyCode.M)) {
            if(music.isPlaying)
            {
                delay = true;
                music.Pause();
            }
            else
            {
                music.Play();
                delay = false;
            }
        }
    }

    public IEnumerator playSong()
    {
        music.clip = musicList[PlayerInfo.Instance.currSong];
        music.Play((ulong) 0.5f);
        yield return new WaitForSeconds(0.6f);
        delay = false;
    }
}
