﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public Button ExitGame;
    public Button StartGame;
    public Button Options;
    public Button Credits;
    public GameObject[] set1;
    public GameObject[] set2;
    public GameObject[] set3;
    public GameObject[] set4;
    public GameObject[] set5;
    public GameObject[] f2b;
    public AudioSource music;
    public Slider musicVol;
    public Slider sfxVol;
    public Toggle freecam;
    public Toggle bangerSongs;
    public Animator blackScreen;
    public Text s1;
    public Text s2;
    public Text s3;

    void Awake()
    {
        LapTracker.lapTracker = 1;
        LapTracker.maxLaps = 3;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        set1 = GameObject.FindGameObjectsWithTag("MenuSet1");
        set2 = GameObject.FindGameObjectsWithTag("MenuSet2");
        set3 = GameObject.FindGameObjectsWithTag("MenuSet3");
        set4 = GameObject.FindGameObjectsWithTag("MenuSet4");
        set5 = GameObject.FindGameObjectsWithTag("MenuSet5");
        f2b = GameObject.FindGameObjectsWithTag("F2B");
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
        foreach (GameObject x in set3)
            x.SetActive(false);
        foreach (GameObject x in set4)
            x.SetActive(false);
        foreach (GameObject x in set5)
            x.SetActive(false);
        foreach (GameObject x in f2b)
            x.SetActive(false);
        if (!PlayerInfo.Instance.tutorial)
        {
            PlayerInfo.Instance.music = (float)0.3;
            PlayerInfo.Instance.sfx = (float)0.35;
            PlayerInfo.Instance.freecam = true;
            PlayerInfo.Instance.bangerSongs = true;
        }
        else
        {
            musicVol.value = PlayerInfo.Instance.music;
            sfxVol.value = PlayerInfo.Instance.sfx;
            freecam.isOn = PlayerInfo.Instance.freecam;
            bangerSongs.isOn = PlayerInfo.Instance.bangerSongs;
            foreach (GameObject x in set1)
                x.SetActive(false);
            foreach (GameObject x in set5)
                x.SetActive(true);
            s1.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[0] / 60), System.Math.Floor(PlayerInfo.Instance.times[0] % 60), PlayerInfo.Instance.times[0] * 1000 % 1000)
            + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[1] / 60), System.Math.Floor(PlayerInfo.Instance.times[1] % 60), PlayerInfo.Instance.times[1] * 1000 % 1000);
            s2.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[2] / 60), System.Math.Floor(PlayerInfo.Instance.times[2] % 60), PlayerInfo.Instance.times[2] * 1000 % 1000)
                + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[3] / 60), System.Math.Floor(PlayerInfo.Instance.times[3] % 60), PlayerInfo.Instance.times[3] * 1000 % 1000);
            s3.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[4] / 60), System.Math.Floor(PlayerInfo.Instance.times[4] % 60), PlayerInfo.Instance.times[4] * 1000 % 1000)
                + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[5] / 60), System.Math.Floor(PlayerInfo.Instance.times[5] % 60), PlayerInfo.Instance.times[5] * 1000 % 1000);
            PlayerInfo.Instance.currSong += 1;
            if (!PlayerInfo.Instance.bangerSongs && PlayerInfo.Instance.currSong > 3)
                PlayerInfo.Instance.currSong = 0;
            if (PlayerInfo.Instance.currSong > 6)
            {
                PlayerInfo.Instance.currSong = 0;
            }
        }
        music.volume = PlayerInfo.Instance.music;

    }

    public void optionsChanged()
    {
        PlayerInfo.Instance.music = musicVol.value;
        PlayerInfo.Instance.sfx = sfxVol.value;
        PlayerInfo.Instance.freecam = freecam.isOn;
        PlayerInfo.Instance.bangerSongs = bangerSongs.isOn;
        music.volume = PlayerInfo.Instance.music;
    }

    public void quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void startGame()
    {
        if (!PlayerInfo.Instance.tutorial)
        {
            foreach (GameObject x in set4)
                x.SetActive(true);
        }
        else
        {
            foreach (GameObject x in set5)
                x.SetActive(true);
            s1.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[0] / 60), System.Math.Floor(PlayerInfo.Instance.times[0] % 60), PlayerInfo.Instance.times[0] * 1000 % 1000)
            + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[1] / 60), System.Math.Floor(PlayerInfo.Instance.times[1] % 60), PlayerInfo.Instance.times[1] * 1000 % 1000);
            s2.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[2] / 60), System.Math.Floor(PlayerInfo.Instance.times[2] % 60), PlayerInfo.Instance.times[2] * 1000 % 1000)
                + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[3] / 60), System.Math.Floor(PlayerInfo.Instance.times[3] % 60), PlayerInfo.Instance.times[3] * 1000 % 1000);
            s3.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[4] / 60), System.Math.Floor(PlayerInfo.Instance.times[4] % 60), PlayerInfo.Instance.times[4] * 1000 % 1000)
                + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[5] / 60), System.Math.Floor(PlayerInfo.Instance.times[5] % 60), PlayerInfo.Instance.times[5] * 1000 % 1000);
        }
        foreach (GameObject x in set1)
            x.SetActive(false);
    }

    public void exitInstructions()
    {
        PlayerInfo.Instance.tutorial = true;
        foreach (GameObject x in set4)
            x.SetActive(false);
        foreach (GameObject x in set5)
            x.SetActive(true);
        s1.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float) System.Math.Floor(PlayerInfo.Instance.times[0] / 60), System.Math.Floor(PlayerInfo.Instance.times[0] % 60), PlayerInfo.Instance.times[0]*1000%1000) 
            + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float) System.Math.Floor(PlayerInfo.Instance.times[1] / 60), System.Math.Floor(PlayerInfo.Instance.times[1] % 60), PlayerInfo.Instance.times[1]*1000%1000);
        s2.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[2] / 60), System.Math.Floor(PlayerInfo.Instance.times[2] % 60), PlayerInfo.Instance.times[2] * 1000 % 1000)
            + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[3] / 60), System.Math.Floor(PlayerInfo.Instance.times[3] % 60), PlayerInfo.Instance.times[3] * 1000 % 1000);
        s3.text = "Personal best: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[4] / 60), System.Math.Floor(PlayerInfo.Instance.times[4] % 60), PlayerInfo.Instance.times[4] * 1000 % 1000)
            + "\nLap Record: " + string.Format("{0:00}:{1:00}:{2:000}", (float)System.Math.Floor(PlayerInfo.Instance.times[5] / 60), System.Math.Floor(PlayerInfo.Instance.times[5] % 60), PlayerInfo.Instance.times[5] * 1000 % 1000);
    }

    public void options()
    {
        foreach (GameObject x in set1)
            x.SetActive(false);
        foreach (GameObject x in set2)
            x.SetActive(true);
    }

    public void backOptions()
    {
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set2)
            x.SetActive(false);
    }
    public void backStage()
    {
        foreach (GameObject x in set1)
            x.SetActive(true);
        foreach (GameObject x in set5)
            x.SetActive(false);
    }

    public void showFreecamInfo()
    {
        set3[0].SetActive(true);
        set3[0].GetComponent<Text>().text = "If disabled, holding shift will always cause the camera to look behind your car instead of using mouse to control your camera.";
    }

    public void showSpeedcoreInfo()
    {
        set3[0].SetActive(true);
        set3[0].GetComponent<Text>().text = "If you disable this, you're uncultured (Just kidding, Disables Camellia's songs from the ingame music. For players who dislike speedcore).";
    }

    public void hideInfo()
    {
        set3[0].SetActive(false);
    }

    public void stage1()
    {
        StartCoroutine(LoadStage(1));
    }
    public void stage2()
    {
        StartCoroutine(LoadStage(2));
    }
    public void stage3()
    {
        StartCoroutine(LoadStage(3));
    }

    public void credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public IEnumerator LoadStage(int num)
    {
        Debug.Log(num);
        foreach (GameObject x in f2b) 
            x.SetActive(true);
        yield return new WaitForSeconds(.1f);
        blackScreen.SetTrigger("Fade2Black");
        yield return new WaitForSeconds(2.2f);
        if(num == 1)
        {
            PlayerInfo.Instance.currStage = 1;
            LapTracker.maxLaps = 3;
            SceneManager.LoadScene("Level 1");
        }
        if (num == 2)
        {
            PlayerInfo.Instance.currStage = 2;
            LapTracker.maxLaps = 2;
            SceneManager.LoadScene("Level 2");
        }
        if (num == 3)
        {
            PlayerInfo.Instance.currStage = 3;
            LapTracker.maxLaps = 2;
            SceneManager.LoadScene("Level 3");
        }
    }
}
