﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PGCTerrain))]
[CanEditMultipleObjects]

public class PGCTerrainEditor2 : Editor
{
    SerializedProperty heightMapScale;
    SerializedProperty width;
    SerializedProperty height;
    SerializedProperty depth;
    SerializedProperty layers;

    bool showRandom = false;
    bool showPerlin = false;
    bool showMultiplePerlin = false;
    bool showSine = false;


    void OnEnable()
    {
        heightMapScale = serializedObject.FindProperty("heightMapScale");
        width = serializedObject.FindProperty("width");
        height = serializedObject.FindProperty("height");
        depth = serializedObject.FindProperty("depth");
        layers = serializedObject.FindProperty("layers");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();

        PGCTerrain terrain = (PGCTerrain)target;

        showRandom = EditorGUILayout.Foldout(showRandom, "Random");
        if (showRandom)
        {
            if (GUILayout.Button("Random Heights"))
            {
                terrain.RandomTerrain();
            }

        }

        showPerlin = EditorGUILayout.Foldout(showPerlin, "Single Perlin Noise");
        if (showPerlin)
        {
            if (GUILayout.Button("Single Perlin Heights"))
            {
                terrain.PerlinTerrain();
            }

        }

        showMultiplePerlin = EditorGUILayout.Foldout(showMultiplePerlin, "Multi Perlin Noise");
        if (showMultiplePerlin)
        {

            EditorGUILayout.PropertyField(layers);

            if (GUILayout.Button("Multi Perlin Heights"))
            {
                terrain.MultiPerlinTerrain();
            }

        }

        showSine = EditorGUILayout.Foldout(showSine, "Sine");
        if (showSine)
        {
            if (GUILayout.Button("Sine Heights"))
            {
                terrain.SineTerrain();
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}