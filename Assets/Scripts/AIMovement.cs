﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMovement : MonoBehaviour
{
    public GameObject path;
    public List<Transform> waypoints = new List<Transform>();
    private Transform targetWaypoint;
    public Rigidbody rb;
    private int targetWaypointIndex = 0;
    private float nodeRange = 10f; //If the distance between the enemy and the waypoint is less than this, then it has reacehd the waypoint
    private int lastWaypointIndex;

    public float movementSpeed = 20f;
    public float rotationSpeed = 20f;

    // Use this for initialization
    void Start()
    {
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        waypoints = new List<Transform>();

        //adding the transforms of the waypoints from the Path object to the list of waypoints
        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                Debug.Log("Add Node" + i + " " + pathTransforms[i].position);
                waypoints.Add(pathTransforms[i]);
            }
        }
        lastWaypointIndex = waypoints.Count - 1;
        targetWaypoint = waypoints[targetWaypointIndex]; //Set the first target waypoint at the start so the enemy starts moving towards a waypoint
    }

    // Update is called once per frame
    void Update()
    {
        float movementStep = movementSpeed * Time.deltaTime;
        float rotationStep = rotationSpeed * Time.deltaTime;

        //points at target
        Vector3 directionToTarget = targetWaypoint.position - transform.position;
        Quaternion rotationToTarget = Quaternion.LookRotation(directionToTarget);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotationToTarget, rotationStep);

        Debug.DrawRay(transform.position, transform.forward * 50f, Color.green, 0f); //Draws a ray forward in the direction the enemy is facing
        Debug.DrawRay(transform.position, directionToTarget, Color.red, 0f); //Draws a ray in the direction of the current target waypoint

        //float distance = Vector3.Distance(transform.position, targetWaypoint.position);
        //CheckDistanceToWaypoint(distance);

        if (Vector3.Distance(transform.position, waypoints[targetWaypointIndex].position) < nodeRange)
        {
            //StartCoroutine(brakingCoroutine());
            if (targetWaypointIndex == waypoints.Count - 1)
            {
                targetWaypointIndex = 0;
            }
            else
            {
                targetWaypointIndex++;
            }

            targetWaypoint = waypoints[targetWaypointIndex];
        }

        //rb.AddForce(transform.forward * movementSpeed);
        transform.position = Vector3.MoveTowards(transform.position, targetWaypoint.position, movementStep);

    }
}
