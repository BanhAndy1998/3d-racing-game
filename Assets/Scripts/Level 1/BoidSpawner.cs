﻿using UnityEngine;

public class BoidSpawner : MonoBehaviour
{
    public static int gameSize = 30;


    private static readonly int numBoids = 10;
    public static GameObject[] allBoids = new GameObject[numBoids];

    public static Vector3 startPos = Vector3.zero;
    public GameObject boidPrefab;

    public bool circleatreeMode;
    public GameObject circleatreePrefab;
    public GameObject circleatreewaypoint1;
    public GameObject circleatreewaypoint2;
    public GameObject circleatreewaypoint3;
    public GameObject circleatreewaypoint4;

    public bool lazyflightMode;
    public GameObject lazyflightPrefab;


    // Start is called before the first frame update
    private void Start()
    {
        //Instantiating numBoids
        for (var i = 0; i < numBoids; i++)
        {
            var pos = new Vector3(Random.Range(-gameSize, gameSize),
                Random.Range(-gameSize, gameSize), Random.Range(-gameSize, gameSize));
            allBoids[i] = Instantiate(boidPrefab, pos, Quaternion.identity);
        }

        if (lazyflightMode == false) lazyflightPrefab.SetActive(false);

        /*if (circleatreeMode == false)
        {
            circleatreePrefab.SetActive(false);
            circleatreewaypoint1.SetActive(false);
            circleatreewaypoint2.SetActive(false);
            circleatreewaypoint3.SetActive(false);
            circleatreewaypoint4.SetActive(false);
        }*/

        circleatreeMode = true;
    }

    //For buttons
    public void LazyflightmodesettoTrue()
    {
        lazyflightMode = true;
        circleatreeMode = false;
        lazyflightPrefab.SetActive(true);
        circleatreePrefab.SetActive(false);
        circleatreewaypoint1.SetActive(false);
        circleatreewaypoint2.SetActive(false);
        circleatreewaypoint3.SetActive(false);
        circleatreewaypoint4.SetActive(false);
        Start();
    }

    public void CircleatreemodesettoTrue()
    {
        circleatreeMode = true;
        lazyflightMode = false;
        circleatreePrefab.SetActive(true);
        circleatreewaypoint1.SetActive(true);
        circleatreewaypoint2.SetActive(true);
        circleatreewaypoint3.SetActive(true);
        circleatreewaypoint4.SetActive(true);
        lazyflightPrefab.SetActive(false);
    }

    private void Update()
    {
        //Sphere object is where boids will go for a random amount of time
        if (lazyflightMode) lazyflight();

        if (circleatreeMode) circleatree();
    }


    private void lazyflight()
    {
        //Fly to random positions with the prefab appearing.
        if (Random.Range(0, 10000) < 150)
        {
            startPos = new Vector3(Random.Range(-gameSize, gameSize), Random.Range(-gameSize, gameSize),
                Random.Range(-gameSize, gameSize));
            lazyflightPrefab.transform.position = startPos;
        }
    }

    private void circleatree()
    {
        //Setting gamesize in case so that boids have enough room for the shape of the square
        gameSize = 30;
        /*All boids are given coordinates for a square shape. Each coordinate is given to
         the boids for 5 seconds*/
        if (Mathf.Repeat(5 - Time.time, 5 + 20) < 5)
            startPos = new Vector3(950, 2,
                923);
        circleatreePrefab.transform.position = startPos;
        /*if (Mathf.Repeat(10 - Time.time, 5 + 20) < 5)
            startPos = new Vector3(0, 0,
                20);
        circleatreePrefab.transform.position = startPos;
        if (Mathf.Repeat(15 - Time.time, 5 + 20) < 5)
            startPos = new Vector3(20, 0,
                20);
        circleatreePrefab.transform.position = startPos;
        if (Mathf.Repeat(20 - Time.time, 5 + 20) < 5)
            startPos = new Vector3(20, 0,
                0);
        circleatreePrefab.transform.position = startPos;*/
    }
}