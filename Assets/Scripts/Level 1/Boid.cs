﻿using UnityEngine;

public class Boid : MonoBehaviour
{
    private readonly float neighborDistance = 30f;

    //Vector3 avoid = new Vector3(5, 5, 5); //Used to change boid avoidance distance 

    private readonly float rotationSpeed = 5.0f;
    private bool edgeofMap;
    public float speed = 3f;


    private void Start()
    {
        speed = Random.Range(3f, 5f);
    }


    private void Update()
    {
        if (Vector3.Distance(transform.position, Vector3.zero) >= BoidSpawner.gameSize
        ) //If your position is greater than the edge of the map then turn back
            edgeofMap = true;
        else
            edgeofMap = false;

        if (edgeofMap)
        {
            var direction = Vector3.zero - transform.position;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),
                rotationSpeed * Time.deltaTime);

            speed = Random.Range(5f, 10f);
        }

        if (Random.Range(0, 10) < 5) boidRules();

        transform.Translate(0, 0, Time.deltaTime * speed); //Makes boids move forward
    }

    //Rules for how boids should be acting such as avoiding each other for a given value and how fast they should go
    private void boidRules()
    {
        GameObject[] bois;
        bois = BoidSpawner.allBoids;

        var vcenter = Vector3.zero;
        var vavoid = Vector3.zero;

        var groupSpeed = 0.1f;

        var startPos = BoidSpawner.startPos;

        float dist;

        var groupSize = 0;

        foreach (var move in bois)
            if (move != gameObject)
            {
                dist = Vector3.Distance(move.transform.position, transform.position);
                if (dist <= neighborDistance)
                {
                    vcenter += move.transform.position;
                    groupSize++;

                    if (dist < 1.0f) vavoid = vavoid + (transform.position - move.transform.position);

                    var newBoid = move.GetComponent<Boid>();
                    groupSpeed = groupSpeed + newBoid.speed;
                }
            }

//Determines how many boids should be allowed in each group
        if (groupSize > 0)
        {
            vcenter = vcenter / groupSize + (startPos - transform.position);
            speed = groupSpeed / groupSize;

            var direction = vcenter + vavoid - transform.position;
            if (direction != Vector3.zero)
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction),
                    rotationSpeed * Time.deltaTime);
        }
    }
}