﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    
    public float speed = 1000;
    public float breakSpeed = 3000;

    public Transform cameraTransform;
    public Rigidbody rb;
    private Transform spawn;

    private float horizontalMovement;
    private float verticalMovement;
    private Vector3 dir;

    //wheels of the car
    public GameObject frontRightWheel;
    public GameObject frontLeftWheel;
    public GameObject backRightWheel;
    public GameObject backLeftWheel;

    public WheelCollider frontRightCollider;
    public WheelCollider frontLeftCollider;
    public WheelCollider backRightCollider;
    public WheelCollider backLeftCollider;

    public bool breaking = false;

    public float turnAngle = 25;    //how much the car can turn

    private void Start()
    {
        //spawn = GameObject.Find("Spawn").transform;
        cameraTransform = Camera.main.transform;
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        horizontalMovement = Input.GetAxis("Horizontal");
        verticalMovement = -1 * Input.GetAxis("Vertical");

        if (Input.GetKey(KeyCode.LeftShift))
        {
            breaking = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            breaking = false;
        }
    }

    private void FixedUpdate()
    {
        backLeftCollider.motorTorque = verticalMovement * speed;
        backRightCollider.motorTorque = verticalMovement * speed;

        frontLeftCollider.steerAngle = turnAngle * horizontalMovement;
        frontRightCollider.steerAngle = turnAngle * horizontalMovement;

        UpdateWheels();

        //hard break, but we can update it to slowly decrease velocity
        if (breaking)
        {
            backRightCollider.brakeTorque = breakSpeed;
            backLeftCollider.brakeTorque = breakSpeed;
        }
        if (breaking == false)
        {
            backRightCollider.brakeTorque = 0;
            backLeftCollider.brakeTorque = 0;
        }

    }

     void UpdateWheels()
    {
        Quaternion rotation;
        Vector3 position;

        frontRightCollider.GetWorldPose(out position, out rotation);
        frontRightWheel.transform.position = position;
        frontRightWheel.transform.rotation = rotation;

        frontLeftCollider.GetWorldPose(out position, out rotation);
        frontLeftWheel.transform.position = position;
        frontLeftWheel.transform.rotation = rotation;

        backRightCollider.GetWorldPose(out position, out rotation);
        backRightWheel.transform.position = position;
        backRightWheel.transform.rotation = rotation;

        backLeftCollider.GetWorldPose(out position, out rotation);
        backLeftWheel.transform.position = position;
        backLeftWheel.transform.rotation = rotation;
    }
}
